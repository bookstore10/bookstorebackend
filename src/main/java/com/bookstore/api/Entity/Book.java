package com.bookstore.api.Entity;

import java.sql.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.GeneratorType;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Tên sách không được trống")
    @Size(min = 2,message = "Tên sách phải có ít nhất 10 kí tự")
    @Column(name = "book_name")
    private String name;

    @NotNull(message = "Mã sách không được trống")
    @Size(min = 2,message = "Mã sách phải có ít nhất 10 kí tự")
    @Column(name = "book_code")
    private String code;

    @NotNull(message = "Đường dẫn ảnh không được trống")
    @Column(name = "cover")
    private String cover;

    @NotNull(message = "Đường dẫn ảnh không được trống")
    @Column(name = "back_cover")
    private String backCover;

    @OneToOne(mappedBy = "book")
    private Price price;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private Type type;

    @NotNull(message = "Số trang sách không được trống")
    @Column(name = "number_page")
    private int numberPage;

    @OneToMany(mappedBy = "book")
    private List<AgeGroup> ageGroup;

    @NotNull(message = "Ngày xuất bản không được trống")
    @Column(name = "public_date")
    private Date publicDate;

    @NotNull(message = "Nhà xuất bản không được trống")
    @Column(name = "publish_company")
    private String publishingCompany;   

    @OneToMany
    @JsonIgnore
    private List<Rating> rating;

    @OneToOne(mappedBy = "book")
    private Order order;

    public Book() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getBackCover() {
        return backCover;
    }

    public void setBackCover(String backCover) {
        this.backCover = backCover;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getNumberPage() {
        return numberPage;
    }

    public void setNumberPage(int numberPage) {
        this.numberPage = numberPage;
    }

    public Date getPublicDate() {
        return publicDate;
    }

    public void setPublicDate(Date publicDate) {
        this.publicDate = publicDate;
    }

    public String getPublishingCompany() {
        return publishingCompany;
    }

    public void setPublishingCompany(String publishingCompany) {
        this.publishingCompany = publishingCompany;
    }

    public List<AgeGroup> getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(List<AgeGroup> ageGroup) {
        this.ageGroup = ageGroup;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public List<Rating> getRating() {
        return rating;
    }

    public void setRating(List<Rating> rating) {
        this.rating = rating;
    }

    
    
    
}
