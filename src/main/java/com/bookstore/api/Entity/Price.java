package com.bookstore.api.Entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "book_price")
public class Price {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Giá theo ngày không được trống")
    @Column(name = "day_price")
    private Integer dayPrice;

    @NotNull(message = "Giá theo tuần không được trống")
    private Integer weekPrice;

    @NotNull(message = "Giá theo tháng không được trống")
    private Integer monthPrice;

    @NotNull(message = "Giá không được trống")
    private Integer price;

    @JoinColumn(name = "book_id")
    @OneToOne
    private Book book;


    public Price() {

    }

    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public Integer getDayPrice() {
        return dayPrice;
    }


    public void setDayPrice(Integer dayPrice) {
        this.dayPrice = dayPrice;
    }


    public Integer getWeekPrice() {
        return weekPrice;
    }


    public void setWeekPrice(Integer weekPrice) {
        this.weekPrice = weekPrice;
    }


    public Integer getMonthPrice() {
        return monthPrice;
    }


    public void setMonthPrice(Integer monthPrice) {
        this.monthPrice = monthPrice;
    }


    public Integer getPrice() {
        return price;
    }


    public void setPrice(Integer price) {
        this.price = price;
    }


    public Book getBook() {
        return book;
    }


    public void setBook(Book book) {
        this.book = book;
    }
    
}
