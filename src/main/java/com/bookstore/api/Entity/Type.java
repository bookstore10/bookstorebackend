package com.bookstore.api.Entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "book_type")
public class Type {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Tên loại sách không được trống")
    @Column(name = "book_type")
    @Size(min = 2,message = "Tên loại sách phải có ít nhất 2 kí tự")
    private String name;

    @NotNull(message = "Mã loại sách không được trống")
    @Column(name = "type_code")
    @Size(min = 2,message = "Mã loại sách phải có ít nhất 2 kí tự")
    private String code;    

    @OneToMany(mappedBy = "type")
    @JsonIgnore
    private List<Book> books;

    @NotNull(message = "Mô tả thể loại sách không được trống")
    @Column(name = "type_description")
    private String description;

    public Type() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

   

}
