package com.bookstore.api.Service;

import com.bookstore.api.Entity.Token;

public interface TokenService {

    Token createToken(Token token);

    Token findByToken(String token);
}
