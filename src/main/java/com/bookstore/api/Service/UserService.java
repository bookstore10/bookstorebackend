package com.bookstore.api.Service;

import com.bookstore.api.Entity.User;
import com.bookstore.api.security.UserPrincipal;

public interface UserService {
    User createUser(User user);

    UserPrincipal findByUsername(String username);
}
