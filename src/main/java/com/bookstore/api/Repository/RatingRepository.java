package com.bookstore.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookstore.api.Entity.Rating;

public interface RatingRepository extends JpaRepository<Rating,Integer> {
    
}
