package com.bookstore.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookstore.api.Entity.Book;

public interface BookRepository extends JpaRepository<Book,Integer>{
    
}
