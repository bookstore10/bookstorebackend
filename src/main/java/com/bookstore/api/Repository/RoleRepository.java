package com.bookstore.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookstore.api.Entity.Role;

public interface RoleRepository extends JpaRepository<Role,Integer> {
    
}
