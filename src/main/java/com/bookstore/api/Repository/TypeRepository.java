package com.bookstore.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookstore.api.Entity.Type;

public interface TypeRepository extends JpaRepository<Type,Integer>{
    
}
