package com.bookstore.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookstore.api.Entity.AgeGroup;

public interface AgeGroupRepository extends JpaRepository<AgeGroup,Integer> {
    
}
