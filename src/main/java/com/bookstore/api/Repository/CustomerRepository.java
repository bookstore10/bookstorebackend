package com.bookstore.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookstore.api.Entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer,Integer> {
    
}
