package com.bookstore.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookstore.api.Entity.Price;

public interface PriceRepository extends JpaRepository<Price,Integer>{
    
}
