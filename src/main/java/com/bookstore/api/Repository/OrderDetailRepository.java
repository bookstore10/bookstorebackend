package com.bookstore.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookstore.api.Entity.Order;

public interface OrderDetailRepository extends JpaRepository<Order,Integer>{
    
}
